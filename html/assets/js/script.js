jQuery(function($){
    $(window).on('resize', function (ev) {
        if( window.innerWidth <= 768 ){
            $('.footer-1 .collapse.show').removeClass('show');
        }else{
            $('.footer-1 .collapse').addClass('show');
        }
    });
    let availableTags = [
        'Cần tây',
        'Món chay',
        'Cà rốt',
        'Trứng gà',
        'Cà chua đút lò',
        'Dưa leo hữu cơ',
        'Thịt heo',
        'Thịt gà',
    ];
    $.widget( "ui.autocomplete", $.ui.autocomplete, {
        _renderMenu: function( ul, items ) {
            var that = this;
            $.each( items, function( index, item ) {
                that._renderItemData( ul, item );
            } );
            ul.prepend('<li class="autocomplete-title" aria-label="title">Tìm kiếm nhiều</li>');
        }
    });


    // const getContext = () => document.getElementById('my-canvas').getContext('2d');
    // const loadImage = (url, id) => {
    //     return new Promise((resolve, reject) => {
    //         const img = new Image();
    //         img.onload = () => resolve(img);
    //         img.onerror = () => reject(new Error(`load ${url} fail`));
    //         img.src = url;
    //         img.id = id;
    //
    //     });
    // };
    // Image_cates.forEach(function(options){
    //     const ctx = getContext();
    //     const myOptions = Object.assign({}, options);
    //     return loadImage(myOptions.image, myOptions.id).then(img => {
    //         ctx.drawImage(img, myOptions.x, myOptions.y, myOptions.sw, myOptions.sh);
    //     });
    // });
    // let _canvas = document.getElementById('my-canvas'),
    // elemLeft = _canvas.offsetLeft,
    // elemTop = _canvas.offsetTop;
    // document.getElementById('my-canvas').addEventListener('mousemove', function(event) {
    //     var x = event.pageX - elemLeft,
    //         y = event.pageY - elemTop;
    //         Image_cates.forEach(function(element){
    //             if (y > element.y && y < element.y + element.sh && x > element.x && x < element.x + element.sw) {
    //                 alert(element.title);
    //             }
    //         })
    // }, false);

    $('.frm-global-search .text-search').autocomplete({
        source: availableTags,
        minLength: 0,
        position: { my: "left top+20", at: "left bottom", collision: "none" },
        open: function ( event, ui ) {
            if( !$('body .autocomplete-ui-overlay').length ){
                $('body').append('<div class="autocomplete-ui-overlay"></div>');
            }
            let x = window.scrollX;
            let y = window.scrollY;
            window.onscroll=function(){window.scrollTo(x, y);};
        },
        close: function( event, ui ) {
            $('body .autocomplete-ui-overlay').remove();
            window.onscroll = function(){};
        }
    }).focus(function(){
        $(this).data("uiAutocomplete").search($(this).val());
    });
    $('.m-frm-global-search .text-search').autocomplete({
        source: availableTags,
        minLength: 0,
        position: { my: "left top+5", at: "left bottom", collision: "none" },
    }).focus(function(){
        $(this).data("uiAutocomplete").search($(this).val());
    });

    $('.h-action-search').click(function(ev){
        ev.preventDefault();
        let $this = $(this), $parent = $this.closest('li');
        $parent.addClass('d-none');
        $('.frm-global-search').removeClass('d-none');
        $('.header .nav-top-2-1').addClass('d-none');
    });
    $(document).on('click', function (e) {
        let $frm_global_search = $(e.target).closest('.frm-global-search');
        let $ui_autocomplete = $(e.target).closest('.ui-autocomplete');
        let $h_action_search = $(e.target).closest('.h-action-search');
        if( !$ui_autocomplete.length && !$frm_global_search.length && !$h_action_search.length ){
            $('.h-action-search').closest('li').removeClass('d-none');
            $('.frm-global-search').addClass('d-none');
            $('.header .nav-top-2-1').removeClass('d-none');
        }
    });
    $('#modal-video').on('shown.bs.modal', function(ev){
        $(this).find('video')[0].play();
    });
    $('#modal-video').on('hide.bs.modal', function(ev){
        $(this).find('video')[0].pause();
    });
    // $('.product-cover-item.product-ci-action').hover(function(){
    //     $(this).addClass('hover');
    //     $(this).closest('.cover-foot-item').find('.product-cover-item:not(.product-ci-action)').addClass('animated');
    // }, function(){
    //     $(this).removeClass('hover');
    //     $(this).closest('.cover-foot-item').find('.product-cover-item:not(.product-ci-action)').removeClass('animated');
    // });
    $('.owl-carousel').each(function(idx, ele){
        let data = $(this).data();
        let options = Object.assign({}, {
            loop: true,
            items: 3,
            nav: false,
            dots: false,
            autoplay: true,
            smartSpeed: 900,
            navText: ['<span aria-label="Previous"><i class="far fa-chevron-left"></i></span>','<span aria-label="Next"><i class="far fa-chevron-right"></i></span>'],
            responsive: {}
        }, data);
        $(this).owlCarousel(options);
    });
    $('.btn-qty-minus').click(function(ev){
        ev.preventDefault();
        let $parent = $(this).closest('.action-qty-content');
        let qty = $('.product-qty', $parent).val();
        qty = Number(qty)
        qty -= 1;
        if( qty < 1 ) {
            qty = 1;
        }
        if( qty <= 1 ){
            $(this).addClass('is-min');
        }else{
            $(this).removeClass('is-min');
        }
        $('.product-qty', $parent).val(qty);
    });
    $('.btn-qty-plus').click(function(ev){
        ev.preventDefault();
        let $parent = $(this).closest('.action-qty-content');
        let qty = $('.product-qty', $parent).val();
        qty = Number(qty)
        qty += 1;
        if( qty < 1 ) {
            qty = 1;
        }
        if( qty <= 1 ){
            $('.btn-qty-minus', $parent).addClass('is-min');
        }else{
            $('.btn-qty-minus', $parent).removeClass('is-min');
        }
        $('.product-qty', $parent).val(qty);
    });

    $('.action-add-to-cart').click(function(ev){
        ev.preventDefault();
        let $parent = $(this).closest('.product-action-add-cart');
        $parent.addClass('d-none');
        $('.product-action-to-cart').removeClass('d-none');
    });
    $('.m-action-to-cart').click(function(ev){
        ev.preventDefault();
        $(this).closest('.modal').modal('hide');
    });
    if( $('.select2').length ) {
        $('.select2').each(function(){
            let $parent = $(this).closest('.modal'), options = {};
            if( $parent.length ){
                options.dropdownParent = $parent
            }
            $(this).select2(options);
        });
    }

    $('.step-checkout').on('show.bs.collapse', function(ev){
        let $currentTarget = $(ev.currentTarget), $header = $currentTarget.find('.collapse-header');
        $header.addClass('is-show');
    });
    $('.step-checkout').on('hide.bs.collapse', function(ev){
        let $currentTarget = $(ev.currentTarget), $header = $currentTarget.find('.collapse-header');
        $header.removeClass('is-show');
    });
    $('.checkout-event-change').change(function(ev){
        let $this = $(this), {parents, handle} = $this.data(), $parent = $this.closest('.item'), $parents = $(parents);
        $parent.find(handle).removeClass('d-none');
        $parents.find('.item').not($parent).find(handle).addClass('d-none');
    });
    $('.js-repeater-add-pay').click(function(ev){
        ev.preventDefault();
        let $this = $(this), $table = $this.closest('table'), $tbody = $table.find('tbody'),
        $tr = $tbody.find('tr:last-child').clone();
        $tr.find('input[type="radio"]').prop('checked', false);
        $tr.find('input[type="text"]').val('');
        $tbody.append($tr);
    });
    $('.js-edit-info').click(function(ev){
        ev.preventDefault();
        let $this = $(this), $parent = $this.closest('.item');
        $parent.find('.info-item').addClass('d-none');
        $this.addClass('d-none');
        $parent.find('.js-save-info').removeClass('d-none');
        $parent.find('.js-delete-info').addClass('d-none');
        $parent.find('.info-item-edit').removeClass('d-none');
    });
    $('.js-save-info').click(function(ev){
        ev.preventDefault();
        let $this = $(this), $parent = $this.closest('.item');
        $parent.find('.info-item-edit').addClass('d-none');
        $this.addClass('d-none');
        $parent.find('.js-edit-info').removeClass('d-none');
        $parent.find('.js-delete-info').removeClass('d-none');
        $parent.find('.info-item').removeClass('d-none');
    });
    $('.js-event-step-checkout').click(function (ev) {
        ev.preventDefault();
        let $this = $(this), {stepCheckout} = $this.data();
        $this.closest('.step-to-step').addClass('d-none');
        $(stepCheckout).removeClass('d-none');
    });
    $('#modal-confirm').on('show.bs.modal', function(ev){
        let $relatedTarget = $(ev.relatedTarget), $this = $(this);
        $('.btn-confirm', $this).off('click').on('click', function(ev){
            $relatedTarget.closest('.item').remove();
            $this.modal('hide');
        });
    });
    $('.btn-reopen-modal').click(function(ev){
        ev.preventDefault();
        let $this = $(this);
        let {target} = $this.data();
        $this.closest('.modal').modal('hide');
        $(target).modal('show');
    });

    $('.back-page.back-step').click(function(ev){
        ev.preventDefault();
        let $parent = $(this).closest('.section-cart.step-to-step'), data = $(this).data();
        $parent.addClass('d-none');
        $(data.target).removeClass('d-none');
    });

    setTimeout(function () {
        $(window).trigger('resize')
    }, 500);
});
const _loaded = (t, e) => {
    t.classList.add("loaded"), e.classList.add("loader-fade");
}

window.addEventListener("load", function() {
    let t = document.body,
        e = document.getElementById("loader");
        _loaded(t, e);
}, !0);
//
// window.addEventListener("load", function() {
//     let t = document.body,
//         e = document.getElementById("loader");
//     if (document.body.classList.contains('homepage')) {
//         setTimeout(function () {
//             _loaded(t, e);
//         }, 2500);
//     }else{
//         _loaded(t, e);
//     }
// }, !0);


