jQuery(function($){
    lightGallery(document.getElementById('gallery-1'), {
        selector: '.gallery-item-image',
    });
    lightGallery(document.getElementById('gallery-2'), {
        selector: '.gallery-item-video',
        videojs: true,
    });
    let $two_image = $('.gallery-list .two-image .gallery-item-image');
    let $one_image = $('.gallery-list .one-image-mobile [data-class="gallery-item-image"]');
    $(window).on('resize', function (ev) {
        let $_clone = $('.gallery-list .item-clone');
        if( window.innerWidth <= 768 ){
            $one_image.each(function(ele, idx){
                let $this = $(this);
                let data = $this.data();
                $this.addClass(data.class);
            });
            $two_image.each(function(ele, idx){
                let $this = $(this);
                let _class = $this.attr('class');
                if( _class ) {
                    $this.attr('data-class', _class);
                    $this.removeClass(_class);
                }
            });
        }else{
            $one_image.each(function(ele, idx){
                let $this = $(this);
                let data = $this.data();
                $this.removeClass(data.class);
            });
            $two_image.each(function(ele, idx){
                let $this = $(this);
                let _class = $this.attr('class') || $this.attr('data-class');
                $this.addClass(_class);
            });
        }
    });
});
